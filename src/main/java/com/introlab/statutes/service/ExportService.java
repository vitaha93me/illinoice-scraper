package com.introlab.statutes.service;

import com.introlab.statutes.domain.Level;
import com.introlab.statutes.domain.Statut;
import com.introlab.statutes.repository.LevelRepository;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class ExportService {

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private StatutService statutService;


    public void export() throws IOException {
        List<Level> level1 = levelRepository.findAllByLevel(1);

        for (Level firstLevel : level1) {

            List<Level> level2 = levelRepository.findAllByParentLevel2AndLevel(firstLevel.getId().getName(), 2);
            for (Level secondLevel : level2) {

                org.jdom2.Document doc = new org.jdom2.Document();
                Namespace namespace = Namespace.getNamespace("stat", "https://casetext.com/stat");
                org.jdom2.Element root = new org.jdom2.Element("Document", namespace);
                root.setAttribute("id", java.util.UUID.randomUUID().toString());
                org.jdom2.Element child1 = new org.jdom2.Element("Title", namespace);
                child1.addContent("Illinois Compiled Statutes");
                org.jdom2.Element child2 = new org.jdom2.Element("URL", namespace);
                child2.addContent("http://www.ilga.gov/legislation/ilcs/ilcs.asp");
                org.jdom2.Element child3 = new org.jdom2.Element("Version", namespace);
                child3.addContent("0.1");
                org.jdom2.Element child4 = new org.jdom2.Element("Currency", namespace);
                child4.addContent("2017-11-13");

                org.jdom2.Element firstLevelLevel = new org.jdom2.Element("Level", namespace);
                firstLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                firstLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" + firstLevel.getId().getName());
                org.jdom2.Element firstLevelText = new org.jdom2.Element("Text", namespace);
                firstLevelLevel.addContent(firstLevelText);
                org.jdom2.Element firstLevelHeading = new org.jdom2.Element("heading", namespace);
                firstLevelText.addContent(firstLevelHeading);
                firstLevelHeading.addContent(firstLevel.getId().getName());
                root.addContent(firstLevelLevel);


                org.jdom2.Element secondLevelLevel = new org.jdom2.Element("Level", namespace);
                secondLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                secondLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" + firstLevel.getId().getName() + "/" + secondLevel.getId().getName());
                org.jdom2.Element secondLevelText = new org.jdom2.Element("Text", namespace);
                secondLevelLevel.addContent(secondLevelText);
                org.jdom2.Element secondLevelHeading = new org.jdom2.Element("heading", namespace);
                secondLevelText.addContent(secondLevelHeading);
                secondLevelHeading.addContent(secondLevel.getId().getName());
                firstLevelLevel.addContent(secondLevelLevel);
                List<Level> level3 = levelRepository.findAllByParentLevel2AndLevel(secondLevel.getId().getName(), 3);
//
                for (Level thirdLevel : level3) {

                    org.jdom2.Element thirdLevelLevel = new org.jdom2.Element("Level", namespace);
                    thirdLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                    thirdLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" + firstLevel.getId().getName() + "/" + secondLevel.getId().getName() + "/" + thirdLevel.getId().getName());
                    org.jdom2.Element thirdLevelText = new org.jdom2.Element("Text", namespace);
                    thirdLevelLevel.addContent(thirdLevelText);
                    org.jdom2.Element thirdLevelHeading = new org.jdom2.Element("heading", namespace);
                    thirdLevelText.addContent(thirdLevelHeading);
                    thirdLevelHeading.addContent(thirdLevel.getId().getName());
                    secondLevelLevel.addContent(thirdLevelLevel);

                    if (thirdLevel.isFinalLevel()) {
                        printStatuteLevel(namespace, thirdLevelLevel, firstLevel, secondLevel, thirdLevel, null, null, null, null);
                    } else {

                        List<Level> level4 = levelRepository.findAllByParentLevel2AndLevel(thirdLevel.getId().getName(), 4);
//
                        for (Level fourthLevel : level4) {

                            org.jdom2.Element fourthLevelLevel = new org.jdom2.Element("Level", namespace);
                            fourthLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                            fourthLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" +
                                    firstLevel.getId().getName() + "/" +
                                    secondLevel.getId().getName() + "/" +
                                    thirdLevel.getId().getName() + "/" +
                                    fourthLevel.getId().getName());
                            org.jdom2.Element fourthLevelText = new org.jdom2.Element("Text", namespace);
                            fourthLevelLevel.addContent(fourthLevelText);
                            org.jdom2.Element fourthLevelHeading = new org.jdom2.Element("heading", namespace);
                            fourthLevelText.addContent(fourthLevelHeading);
                            fourthLevelHeading.addContent(fourthLevel.getId().getName());
                            thirdLevelLevel.addContent(fourthLevelLevel);

                            if (fourthLevel.isFinalLevel()) {
                                printStatuteLevel(namespace, fourthLevelLevel, firstLevel, secondLevel, thirdLevel, fourthLevel, null, null, null);
                            } else {

                                List<Level> level5 = levelRepository.findAllByParentLevel2AndLevel(fourthLevel.getId().getName(), 5);
//
                                for (Level fivesLevel : level5) {

                                    org.jdom2.Element fivesLevelLevel = new org.jdom2.Element("Level", namespace);
                                    fivesLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                                    fivesLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" +
                                            firstLevel.getId().getName() + "/" +
                                            secondLevel.getId().getName() + "/" +
                                            thirdLevel.getId().getName() + "/" +
                                            fourthLevel.getId().getName() + "/" +
                                            fivesLevel.getId().getName());
                                    org.jdom2.Element fivesLevelText = new org.jdom2.Element("Text", namespace);
                                    fivesLevelLevel.addContent(fivesLevelText);
                                    org.jdom2.Element fivesLevelHeading = new org.jdom2.Element("heading", namespace);
                                    fivesLevelText.addContent(fivesLevelHeading);
                                    fivesLevelHeading.addContent(fivesLevel.getId().getName());
                                    fourthLevelLevel.addContent(fivesLevelLevel);

                                    if (fivesLevel.isFinalLevel()) {
                                        printStatuteLevel(namespace, fivesLevelLevel, firstLevel, secondLevel, thirdLevel, fourthLevel, fivesLevel, null, null);
                                    } else {

                                        List<Level> level6 = levelRepository.findAllByParentLevel2AndLevel(fivesLevel.getId().getName(), 6);
                                        for (Level sixLevel : level6) {

                                            org.jdom2.Element sixLevelLevel = new org.jdom2.Element("Level", namespace);
                                            fivesLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                                            fivesLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" +
                                                    firstLevel.getId().getName() + "/" +
                                                    secondLevel.getId().getName() + "/" +
                                                    thirdLevel.getId().getName() + "/" +
                                                    fourthLevel.getId().getName() + "/" +
                                                    fivesLevel.getId().getName() + "/" +
                                                    sixLevel.getId().getName());
                                            org.jdom2.Element sixLevelText = new org.jdom2.Element("Text", namespace);
                                            sixLevelLevel.addContent(sixLevelText);
                                            org.jdom2.Element sixLevelHeading = new org.jdom2.Element("heading", namespace);
                                            sixLevelText.addContent(sixLevelHeading);
                                            sixLevelHeading.addContent(sixLevel.getId().getName());
                                            fivesLevelLevel.addContent(sixLevelLevel);

                                            if (sixLevel.isFinalLevel()) {
                                                printStatuteLevel(namespace, sixLevelLevel, firstLevel, secondLevel, thirdLevel, fourthLevel, fivesLevel, sixLevel, null);
                                            } else {

                                                List<Level> level7 = levelRepository.findAllByParentLevel2AndLevel(fivesLevel.getId().getName(), 7);
                                                for (Level seventhLevel : level7) {

                                                    org.jdom2.Element seventhLevelLevel = new org.jdom2.Element("Level", namespace);
                                                    seventhLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
                                                    seventhLevelLevel.setAttribute("path", "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" +
                                                            firstLevel.getId().getName() + "/" +
                                                            secondLevel.getId().getName() + "/" +
                                                            thirdLevel.getId().getName() + "/" +
                                                            fourthLevel.getId().getName() + "/" +
                                                            fivesLevel.getId().getName() + "/" +
                                                            sixLevel.getId().getName());
                                                    org.jdom2.Element seventhLevelText = new org.jdom2.Element("Text", namespace);
                                                    seventhLevelLevel.addContent(seventhLevelText);
                                                    org.jdom2.Element seventhLevelHeading = new org.jdom2.Element("heading", namespace);
                                                    seventhLevelText.addContent(seventhLevelHeading);
                                                    seventhLevelHeading.addContent(seventhLevel.getId().getName());
                                                    sixLevelLevel.addContent(seventhLevelLevel);

                                                    printStatuteLevel(namespace, secondLevelLevel, firstLevel, secondLevel, thirdLevel, fourthLevel, fivesLevel, sixLevel, seventhLevel);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                doc.setRootElement(root);

                XMLOutputter outter = new XMLOutputter();
                outter.setFormat(Format.getPrettyFormat());
                outter.output(doc, new FileWriter(new File(secondLevel.getId().getName() + ".xml")));
                System.out.println(secondLevel.getId().getName());
            }

        }
    }

    private void printStatuteLevel(Namespace namespace, org.jdom2.Element thirdLevelLevel, Level firstLevel, Level secondLevel, Level thirdLevel, Level fourthLevel, Level fivesLevel, Level sixLevel, Level seventhLevel) {
        Level statutParent = seventhLevel != null ? seventhLevel : sixLevel != null ? sixLevel : fivesLevel != null ? fivesLevel : fourthLevel != null ? fourthLevel : thirdLevel;
        for (Statut statut : statutService.findAllByParentLevelAndLink(statutParent.getId().getName(), statutParent.getLink())) {

            org.jdom2.Element statutLevelLevel = new org.jdom2.Element("Level", namespace);
            statutLevelLevel.setAttribute("id", java.util.UUID.randomUUID().toString());
            String path = "/us/illinoiscompiledstatutes/Illinois Compiled Statutes/" +
                    firstLevel.getId().getName() + "/" +
                    secondLevel.getId().getName() + "/" +
                    thirdLevel.getId().getName();

            if (fourthLevel != null) {
                path += "/" + fourthLevel.getId().getName();
            }
            if (fivesLevel != null) {
                path += "/" + fivesLevel.getId().getName();
            }
            if (sixLevel != null) {
                path += "/" + sixLevel.getId().getName();
            }
            if (seventhLevel != null) {
                path += "/" + seventhLevel.getId().getName();
            }

            ;
            path += "/Sec. " + StringUtils.substringAfter(statut.getCode(), "/").trim();
            statutLevelLevel.setAttribute("path", path);
            statutLevelLevel.setAttribute("is_citable", "true");
            org.jdom2.Element statutLevelCitation = new org.jdom2.Element("Citation", namespace);
            statutLevelCitation.addContent(statut.getCode());
            org.jdom2.Element statutLevelText = new org.jdom2.Element("Text", namespace);
            statutLevelLevel.addContent(statutLevelCitation);
            statutLevelLevel.addContent(statutLevelText);
            org.jdom2.Element statutLevelHeading = new org.jdom2.Element("heading", namespace);
            org.jdom2.Element statutLevelNote = new org.jdom2.Element("note", namespace);
            org.jdom2.Element statutLevelSource = new org.jdom2.Element("note", namespace);
            statutLevelText.addContent(statutLevelHeading);
            statutLevelHeading.addContent(statut.getSectionName());
            statutLevelNote.addContent(statut.getNoteFrom());
            statutLevelSource.addContent(statut.getNoteSource());


            statutLevelText.addContent(statutLevelNote);

            String paragraphs = statut.getParagraphs();
            if (paragraphs != null && !paragraphs.isEmpty()) {
                Arrays.stream(paragraphs.split("!!delimeter!!"))
                        .filter(paragraph -> paragraph.length() > 5)
                        .forEach(paragraph -> {
                            org.jdom2.Element statutLevelParagraph = new org.jdom2.Element("p", namespace);
                            statutLevelParagraph.addContent(paragraph);
                            statutLevelText.addContent(statutLevelParagraph);
                        });
            }

            statutLevelText.addContent(statutLevelSource);
            thirdLevelLevel.addContent(statutLevelLevel);

        }
    }
}
