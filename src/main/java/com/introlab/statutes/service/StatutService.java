package com.introlab.statutes.service;

import com.introlab.statutes.domain.Statut;
import com.introlab.statutes.repository.StatutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class StatutService {
    private final StatutRepository repository;

    @Autowired
    public StatutService(StatutRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void removeByParentLevelAndLink(String level, String link) {
        repository.deleteAllByParentLevelAndLink(level, link);
    }

    public void save(List<Statut> statutes) {
        repository.save(statutes);
    }

    public List<Statut> findAllByParentLevelAndLink(String level, String link) {
        return repository.findAllByParentLevelAndLink(level, link);
    }
}
