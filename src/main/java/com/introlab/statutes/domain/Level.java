package com.introlab.statutes.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity
public class Level implements Serializable {

    @EmbeddedId
    private LevelId id;
    private String link;
    private String parentLevel2;
    private boolean finalLevel;

    private int level;


}
