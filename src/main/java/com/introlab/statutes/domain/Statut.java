package com.introlab.statutes.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * @author vitalii.
 */

@Setter
@Getter
@Entity
public class Statut {

    @Id
    private String code;

    private String noteFrom = "";
    private String noteSource;
    private String sectionName;

    private boolean finalNode;
    private String link;
    private String currentLevel;
    private String parentLevel;
    private String path;

    @Lob
    private String paragraphs;
}
