package com.introlab.statutes.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author vitalii.
 */
@Embeddable
public class LevelId implements Serializable {

    @Column(name = "name")
    private String name;

    @Column(name = "parentLevel")
    private String parentLevel;

    @Column(name = "link2")
    private String link;

    public LevelId() {
    }

    public LevelId(String name, String parentLevel, String link) {
        this.name = name;
        this.parentLevel = parentLevel;
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public String getParentLevel() {
        return parentLevel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentLevel(String parentLevel) {
        this.parentLevel = parentLevel;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LevelId)) return false;
        LevelId that = (LevelId) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getLink(), that.getLink()) &&
                Objects.equals(getParentLevel(), that.getParentLevel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getParentLevel());
    }
}
