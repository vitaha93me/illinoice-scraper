package com.introlab.statutes.scraper;

import com.introlab.statutes.domain.Level;
import com.introlab.statutes.domain.LevelId;
import com.introlab.statutes.domain.Statut;
import com.introlab.statutes.repository.LevelRepository;
import com.introlab.statutes.service.ExportService;
import com.introlab.statutes.service.StatutService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author vitalii.
 */
@Component
public class Scraper {

    private final LevelRepository levelRepository;

    private final StatutService statutService;

    private final ExportService exportService;

    @Autowired
    public Scraper(LevelRepository levelRepository, StatutService statutService, ExportService exportService) {
        this.levelRepository = levelRepository;
        this.statutService = statutService;
        this.exportService = exportService;
    }

    public void scrape() throws IOException {
        Document rootDocument = Jsoup.connect("http://www.ilga.gov/legislation/ilcs/ilcs.asp").get();
        saveFirstTwoLevels(rootDocument);
        saveThirdAndFoursLevel();
        processFourthLevel();
        exportService.export();
    }


    private void processFourthLevel() throws IOException {
        List<Level> thirdLevels = levelRepository.findForScrapeFourthLevel();
        thirdLevels.addAll(levelRepository.findAllByLevelAndLinkIsNotNull(3));

        for (Level thirdLevel : thirdLevels) {
            if (thirdLevel.getId().getName().equals("215 ILCS 5/ Illinois Insurance Code.")) {
                continue;
            }
            System.out.println(thirdLevel.getLink());
            Document document = getDocument(thirdLevel);
            if (document.text().contains("View Entire Act")) {

                if (!document.select(".indent50").isEmpty()) {

                    Elements select = document.select("#toplinks ~ div:has(a)");

                    Level currentLevel1 = new Level();
                    Level currentLevel2 = new Level();

                    for (Element element : select) {

                        if (element.className().equals("indent10")) {
                            Level level = new Level();

                            if (!select.get(select.indexOf(element) + 1).className().equals("indent10") &&
                                    !select.get(select.indexOf(element) + 1).className().equals("content")) {

                                String link = element.select("a").isEmpty() ? "" : element.select("a").attr("abs:href");

                                level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                        .replace("&nbsp;ILCS", " ILCS")
                                        .replace("&nbsp;", ""), thirdLevel.getId().getName(),
                                        link));

                                level.setLevel(thirdLevel.getLevel() + 1);
                                level.setParentLevel2(thirdLevel.getId().getName());
                                levelRepository.save(level);
                                System.out.println("  " + level.getId().getName());
                                currentLevel1 = level;
                            } else {
                                String link = element.select("a").attr("abs:href");
                                level.setLink(link);

                                level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                        .replace("&nbsp;ILCS", " ILCS")
                                        .replace("&nbsp;", ""), thirdLevel.getId().getName(), link));

                                level.setLevel(thirdLevel.getLevel() + 1);
                                level.setParentLevel2(thirdLevel.getId().getName());
                                level.setFinalLevel(true);
                                System.out.println("  " + level.getId().getName());
                                levelRepository.save(level);
                                String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                                saveStatutes(level, getDocument(level), subCharter);
                            }

                        } else if (element.className().equals("indent25")) {
                            Level level = new Level();

                            if (!select.get(select.indexOf(element) + 1).className().equals("indent25") &&
                                    !select.get(select.indexOf(element) + 1).className().equals("content") &&
                                    !select.get(select.indexOf(element) + 1).className().equals("indent10")) {
                                String link = element.select("a").isEmpty() ? "" : element.select("a").attr("abs:href");

                                level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                        .replace("&nbsp;ILCS", " ILCS")
                                        .replace("&nbsp;", ""), currentLevel1.getId().getName(), link));

                                level.setLevel(currentLevel1.getLevel() + 1);
                                level.setParentLevel2(currentLevel1.getId().getName());
                                levelRepository.save(level);
                                System.out.println("        " + level.getId().getName());

                                currentLevel2 = level;
                            } else {
                                String link = element.select("a").attr("abs:href");
                                level.setLink(link);

                                level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                        .replace("&nbsp;ILCS", " ILCS")
                                        .replace("&nbsp;", ""), currentLevel1.getId().getName(), link));

                                level.setLevel(currentLevel1.getLevel() + 1);
                                level.setParentLevel2(currentLevel1.getId().getName());
                                level.setFinalLevel(true);
                                levelRepository.save(level);
                                System.out.println("        " + level.getId().getName());

                                String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                                saveStatutes(level, getDocument(level), subCharter);
                            }
                        } else if (element.className().equals("indent50")) {
                            Level level = new Level();

                            String link = element.select("a").attr("abs:href");
                            level.setLink(link);

                            level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                    .replace("&nbsp;ILCS", " ILCS")
                                    .replace("&nbsp;", ""), currentLevel2.getId().getName(), link));

                            level.setLevel(currentLevel2.getLevel() + 1);
                            level.setParentLevel2(currentLevel2.getId().getName());
                            level.setFinalLevel(true);
                            levelRepository.save(level);
                            System.out.println("                " + level.getId().getName());

                            String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                            saveStatutes(level, getDocument(level), subCharter);
                        }
                    }


                } else if (!document.select(".indent25").isEmpty()) {

                    Elements select = document.select("#toplinks ~ div:has(a)");
                    Level currentLevel1 = new Level();

                    for (Element element : select) {
                        if (element.className().equals("indent10")) {
                            Level level = new Level();

                            if (!select.get(select.indexOf(element) + 1).className().equals("indent10") &&
                                    !select.get(select.indexOf(element) + 1).className().equals("content")) {
                                String link = element.select("a").isEmpty() ? "" : element.select("a").attr("abs:href");

                                level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                        .replace("&nbsp;ILCS", " ILCS")
                                        .replace("&nbsp;", ""), thirdLevel.getId().getName(), link));

                                level.setLevel(thirdLevel.getLevel() + 1);
                                level.setParentLevel2(thirdLevel.getId().getName());
                                levelRepository.save(level);
                                currentLevel1 = level;
                                System.out.println("  " + level.getId().getName());

                            } else {
                                String link = element.select("a").attr("abs:href");
                                level.setLink(link);

                                level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                        .replace("&nbsp;ILCS", " ILCS")
                                        .replace("&nbsp;", ""), thirdLevel.getId().getName(), link));

                                level.setLevel(thirdLevel.getLevel() + 1);
                                level.setParentLevel2(thirdLevel.getId().getName());
                                level.setFinalLevel(true);
                                levelRepository.save(level);
                                System.out.println("  " + level.getId().getName());

                                String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                                saveStatutes(level, getDocument(level), subCharter);
                            }

                        } else if (element.className().equals("indent25")) {
                            Level level = new Level();
                            String link = element.select("a").attr("abs:href");
                            level.setLink(link);

                            level.setId(new LevelId(StringUtils.substring(element.text(), 0, 254)
                                    .replace("&nbsp;ILCS", " ILCS")
                                    .replace("&nbsp;", ""), currentLevel1.getId().getName(), link));

                            level.setLevel(currentLevel1.getLevel() + 1);
                            level.setParentLevel2(currentLevel1.getId().getName());
                            level.setFinalLevel(true);
                            levelRepository.save(level);
                            System.out.println("        " + level.getId().getName());

                            String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                            saveStatutes(level, getDocument(level), subCharter);
                        }
                    }
                } else {
                    Elements select = document.select("#toplinks ~ div a:not([href='#top'])");

                    for (Element element : select) {
                        String link = element.attr("abs:href");
                        String name = element.text();
                        Level level = new Level();
                        level.setLink(link);
                        level.setId(new LevelId(StringUtils.substring(name, 0, 254)
                                .replace("&nbsp;ILCS", " ILCS")
                                .replace("&nbsp;", ""), thirdLevel.getId().getName(), link));

                        level.setLevel(thirdLevel.getLevel() + 1);
                        level.setParentLevel2(thirdLevel.getId().getName());
                        level.setFinalLevel(true);
                        levelRepository.save(level);
                        String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                        saveStatutes(level, getDocument(level), subCharter);
                    }
                }

            } else {
                thirdLevel.setFinalLevel(true);
                levelRepository.save(thirdLevel);
                String subCharter = StringUtils.substringBefore(thirdLevel.getId().getName(), "/");
                saveStatutes(thirdLevel, document, subCharter);
            }
        }
    }

    private Document getDocument(Level level) {
        System.out.println("fetch url\t" + level.getLink());
        try {
            return Jsoup.connect(level.getLink()).maxBodySize(0).timeout(6000000).get();
        } catch (Throwable e) {
            errorCount++;
            System.out.println(errorCount);
            e.printStackTrace();
            return Jsoup.parse("");
        }
    }

    private static int errorCount;

    private void saveStatutes(Level fourthLevel, Document document, String subChapter) {

        System.out.println(fourthLevel.getLink());

        Elements source = document.select(".heading ~ *");
        String sourceHtml = source.html().replace("    ", ",,").replace("<br>", ",,").replaceAll("(,,)+", ",,");
        String text = Jsoup.parse(sourceHtml).text();

        text = text.replaceAll("<.*?>", "");

        List<String> citations = Arrays.asList(text.split("(?=(,,\\s*\\(" + subChapter + ".*?\\)))"));
        List<Statut> statutes = new ArrayList<>();
        if (citations.isEmpty()) {
            return;
        }
        for (String citation : citations) {
            if (!citation.contains("Sec.")) {
                continue;
            }
            if (citation.contains("SUBSECTION") && !statutes.isEmpty()) {
                statutes.get(statutes.size() - 1).setParagraphs(statutes.get(statutes.size() - 1).getParagraphs() + citation.replaceAll("(,, *)+", "!!delimeter!!"));
                continue;
            }

            citation = citation.replace("&nbsp;", "");
            Statut statut = new Statut();

            if (citation.contains("(from Ch.")) {
                String code = StringUtils.substringBefore(citation, "(from Ch.");
                citation = citation.replace(code, "");
                statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                String note = citation.split("(?<=(\\(from[A-Za-z0-9 \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                statut.setNoteFrom(note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");
            }

            if (citation.contains("(Text of Section")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(Text of Section");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(Text of Section[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");
            }

            if (citation.contains("(This Act was repealed")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(This Act was repealed");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(This Act was repealed[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(This Act was approved")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(This Act was approved");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(This Act was approved[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,100}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(This Section may contain")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(This Section may contain");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(This Section may contain[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,100}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(Section") && !citation.contains("720 ILCS 5") && !citation.contains("60 ILCS 1") && !citation.contains("215 ILCS 5") && !citation.contains("40 ILCS 5")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(Section");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(Section[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            } else {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "Sec.");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                    citation = citation.replace(code, "");
                }
                if (citation.isEmpty()) {
                    continue;
                }
                if (citation.charAt(0) == ' ' || citation.charAt(0) == ')') {
                    citation = citation.substring(1, citation.length() - 1);
                }
                if (citation.charAt(1) == ' ' || citation.charAt(1) == ')') {
                    citation = citation.substring(1, citation.length() - 1);
                }
            }

            System.out.println("            " + statut.getCode());
            String[] split = citation.split("(?<=(Sec\\.[A-Za-z\\(\\)0-9 \\.\\-;',]{1,100}\\. ))");
            if (split.length < 2) {
                String sectionName = StringUtils.substringBefore(citation, "(Source");
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(sectionName, "");
            } else if (!split[1].contains("(a)") && !split[1].contains("(Source:") && split[1].length() < 150) {
                String sectionName = split[0] + " " + split[1];
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(split[0], "").replace(split[1], "");
            } else if (split[1].contains("(a)") || split[1].contains("(Source:")) {
                String sectionName = split[0];
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(split[0], "");
            } else if (split[0].contains("(Source:")) {
                String sectionName = StringUtils.substringBefore(split[0], "(Source:");
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(sectionName, "");
            } else {
                statut.setSectionName(split[0].replaceAll(",,", ""));
                citation = citation.replace(split[0], "");
            }

            Pattern pattern = Pattern.compile("\\(Source:[A-Za-z0-9; ,\\-\\.\\/]{5,50}\\)");
            Matcher matcher = pattern.matcher(citation);
            if (matcher.find()) {

                String note = matcher.group();
                citation = StringUtils.substringBefore(citation, note);
                if (note.charAt(0) == '(' && note.charAt(note.length() - 1) == ')')
                    statut.setNoteSource(note.substring(1, note.length() - 2));
            }


            statut.setParagraphs(citation);
            statut.setCurrentLevel(statut.getCode());
            statut.setParentLevel(fourthLevel.getId().getName());
//
            if (statut.getSectionName().startsWith(")")) {
                statut.setSectionName(StringUtils.substring(statut.getSectionName(), 2));
            }

            System.out.println("Section:\t" + statut.getSectionName());
            System.out.println("NoteFrom:\t" + statut.getNoteFrom());
            System.out.println("NoteSource:\t" + statut.getNoteSource());
            System.out.println();
            statut.setSectionName(statut.getSectionName().replaceAll("\\s+", " "));
            if (statut.getNoteFrom() != null)
                statut.setNoteFrom(statut.getNoteFrom().replaceAll("(,, *)+", " "));
            if (statut.getNoteSource() != null)
                statut.setNoteSource(statut.getNoteSource().replaceAll("(,, *)+", " "));
            if (statut.getParagraphs() != null) {
                statut.setParagraphs(statut.getParagraphs().replaceAll("(,, *)+", "!!delimeter!!"));
            }

            if (statut.getCode().contains("SUBSECTION")) {
                continue;
            }
            statut.setLink(fourthLevel.getLink());
            statutes.add(statut);
        }

        if (statutes.isEmpty()) {
            return;
        }
        statutService.removeByParentLevelAndLink(statutes.get(0).getParentLevel(), statutes.get(0).getLink());
        statutService.save(statutes);
    }

    private void saveThirdAndFoursLevel() throws IOException {

        List<Level> secondLevels = levelRepository.findAllByLevel(2);
        for (Level secondLevel : secondLevels) {
            List<Level> thirdLevels = new ArrayList<>();
            List<Level> fourthLevels = new ArrayList<>();

            System.out.println(secondLevel.getId().getName());
            System.out.println(secondLevel.getLink());
            Document document = Jsoup.connect(secondLevel.getLink()).header("Accept-Encoding", "gzip, deflate")
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0")
                    .maxBodySize(0)
                    .get();


            if (secondLevel.getLink().equals("http://www.ilga.gov/legislation/ilcs/ilcs2.asp?ChapterID=58")) {
                System.out.println(document.select(".heading ~ table ul > li"));
            }
            if (document.select(".heading ~ table ul > p").isEmpty()) {
                Elements select = document.select(".heading ~ table ul > li, .heading table ul > li");
                for (Element element : select) {
                    System.out.println("inside this hernia");

                    String link = element.select("a").attr("abs:href");
                    String name = element.text();
                    Level level = new Level();
                    level.setLink(link);
                    level.setId(new LevelId(StringUtils.substring(name, 0, 254)
                            .replace("&nbsp;ILCS", " ILCS")
                            .replace("&nbsp;", ""), secondLevel.getId().getName(), link));

                    level.setLevel(3);
                    level.setParentLevel2(secondLevel.getId().getName());
                    thirdLevels.add(level);
                    System.out.println("    " + level.getId().getName());
                }
                levelRepository.save(thirdLevels);
            } else {

                String currentLevel = "";
                Elements select = document.select(".heading ~ table ul > *");
                for (Element element : select) {
                    if (element.tagName().equals("p")) {
                        currentLevel = element.text();
                        Level level = new Level();
                        level.setLevel(3);
                        level.setParentLevel2(secondLevel.getId().getName());
                        level.setId(new LevelId(currentLevel.replace("&nbsp;", ""), level.getParentLevel2(), ""));
                        thirdLevels.add(level);
                        System.out.println(currentLevel);
                    } else if (element.tagName().equals("li")) {
                        String link = element.select("a").attr("abs:href");
                        String name = element.text();
                        Level level = new Level();
                        level.setLink(link);
                        level.setId(new LevelId(StringUtils.substring(name, 0, 254)
                                .replace("&nbsp;ILCS", " ILCS")
                                .replace("&nbsp;", ""), currentLevel, link));

                        level.setLevel(4);
                        level.setParentLevel2(currentLevel);
                        fourthLevels.add(level);
                        System.out.println("    " + level.getId().getName());
                    }
                }
                levelRepository.save(thirdLevels);
                levelRepository.save(fourthLevels);
            }
        }

    }

    private void saveFirstTwoLevels(Document document) {

        List<Level> firstLevels = new ArrayList<>();
        List<Level> secondLevels = new ArrayList<>();

        String currentLevel = "";
        for (Element element : document.select("center ul > *")) {
            if (element.tagName().equals("div")) {
                currentLevel = element.text();
                Level level = new Level();
                level.setLevel(1);
                String link = element.select("a").isEmpty() ? "" : element.select("a").attr("abs:href");

                level.setId(new LevelId(currentLevel.replace("&nbsp;", ""), "", link));

                firstLevels.add(level);
                System.out.println(currentLevel);
            } else if (element.tagName().equals("li")) {
                String link = element.select("a").attr("abs:href");
                String name = element.text();
                Level level = new Level();
                level.setId(new LevelId(name.replace("&nbsp;", ""), currentLevel, link));

                level.setLink(link);
                level.setLevel(2);
                level.setParentLevel2(currentLevel);
                secondLevels.add(level);
                System.out.println("    " + name);
            }
        }

        levelRepository.save(firstLevels);
        levelRepository.save(secondLevels);
    }
}
