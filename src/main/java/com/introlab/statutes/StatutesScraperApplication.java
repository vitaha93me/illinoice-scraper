package com.introlab.statutes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatutesScraperApplication {

    public static void main(String[] args) {
        SpringApplication.run(StatutesScraperApplication.class, args);
    }
}
