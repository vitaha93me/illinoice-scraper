package com.introlab.statutes.repository;

import com.introlab.statutes.domain.Statut;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author vitalii.
 */
public interface StatutRepository extends JpaRepository<Statut, String> {

    void deleteAllByParentLevelAndLink(String level, String link);

    List<Statut> findAllByParentLevelAndLink(String level, String link);
}
