package com.introlab.statutes.repository;

import com.introlab.statutes.domain.Level;
import com.introlab.statutes.domain.LevelId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author vitalii.
 */
public interface LevelRepository extends JpaRepository<Level, LevelId> {

    List<Level> findAllByLevel(int level);

    List<Level> findAllByLevelAndLinkIsNotNull(int level);

    @Query(nativeQuery = true, value = "SELECT * FROM level WHERE name REGEXP '^[ 0-9]' AND level=4")
    List<Level> findForScrapeFourthLevel();

    List<Level> findAllByParentLevel2AndLevel(String parent, int dept);

    List<Level> findAllByIdNameAndLevel(String parent, int dept);

    List<Level> findAllByParentLevel2(String parent);

}
