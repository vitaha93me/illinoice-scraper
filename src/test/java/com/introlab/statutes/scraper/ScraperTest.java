package com.introlab.statutes.scraper;

import com.introlab.statutes.StatutesScraperApplicationTests;
import com.introlab.statutes.domain.Statut;
import com.introlab.statutes.service.StatutService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author vitalii.
 */
public class ScraperTest extends StatutesScraperApplicationTests {

    @Autowired
    StatutService statutService;

    @Test
    public void test() throws IOException {

        Document document = Jsoup.connect("http://www.ilga.gov/legislation/ilcs/ilcs3.asp?ActID=1189&ChapterID=20").get();

        Elements source = document.select(".heading ~ *");
        String sourceHtml = source.html().replace("    ", ",,").replace("<br>", ",,").replaceAll("(,,)+", ",,");
        String text = Jsoup.parse(sourceHtml).text();

        text = text.replaceAll("<.*?>", "");

        List<String> citations = Arrays.asList(text.split("(?=(,,\\(205 ILCS 605.*?\\)))"));
        List<Statut> statutes = new ArrayList<>();
        if (citations == null || citations.isEmpty()) {
            return;
        }
        for (String citation : citations) {
            if (!citation.contains("Sec.")) {
                continue;
            }
            if (citation.contains("SUBSECTION") && !statutes.isEmpty()) {
                statutes.get(statutes.size() - 1).setParagraphs(statutes.get(statutes.size() - 1).getParagraphs() + citation.replaceAll("(,, *)+", "!!delimeter!!"));
                continue;
            }

            citation = citation.replace("&nbsp;", "");
            Statut statut = new Statut();

            if (citation.contains("(from Ch.")) {
                String code = StringUtils.substringBefore(citation, "(from Ch.");
                citation = citation.replace(code, "");
                statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                String note = citation.split("(?<=(\\(from[A-Za-z0-9 \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                statut.setNoteFrom(note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");
            }

            if (citation.contains("(Text of Section")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(Text of Section");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(Text of Section[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(This Act was repealed")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(This Act was repealed");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(This Act was repealed[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(This Act was approved")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(This Act was approved");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(This Act was approved[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,100}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(This Section may contain")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(This Section may contain");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(This Section may contain[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,100}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            }

            if (citation.contains("(Section") && !citation.contains("720 ILCS 5") && !citation.contains("60 ILCS 1") && !citation.contains("215 ILCS 5") && !citation.contains("40 ILCS 5")) {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "(Section");
                    citation = citation.replace(code, "");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                }
                String note = citation.split("(?<=(\\(Section[A-Za-z0-9; \\)\\(,\\-\\.\\/]{5,50}\\)))")[0];
                if (note.contains("Sec. ")) {
                    note = StringUtils.substringBefore(note, "Sec. ");
                }
                statut.setNoteFrom(statut.getNoteFrom() + note.replace("(", "").replace(")", "").replaceAll(",,", ""));
                citation = citation.replace(note, "");

            } else {
                if (statut.getCode() == null || statut.getCode().isEmpty()) {
                    String code = StringUtils.substringBefore(citation, "Sec.");
                    statut.setCode(code.replace("(", "").replace(")", "").replaceAll(",,", ""));
                    citation = citation.replace(code, "");
                }
                if (citation.isEmpty()) {
                    continue;
                }
                if (citation.charAt(0) == ' ' || citation.charAt(0) == ')') {
                    citation = citation.substring(1, citation.length() - 1);
                }
                if (citation.charAt(1) == ' ' || citation.charAt(1) == ')') {
                    citation = citation.substring(1, citation.length() - 1);
                }
            }

            System.out.println("            " + statut.getCode());
            String[] split = citation.split("(?<=(Sec\\.[A-Za-z\\(\\)0-9 \\.\\-;',]{1,100}\\. ))");
            if (split.length < 2) {
                String sectionName = StringUtils.substringBefore(citation, "(Source");
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(sectionName, "");
            } else if (!split[1].contains("(a)") && !split[1].contains("(Source:") && split[1].length() < 150) {
                String sectionName = split[0] + " " + split[1];
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(split[0], "").replace(split[1], "");
            } else if (split[1].contains("(a)") || split[1].contains("(Source:")) {
                String sectionName = split[0];
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(split[0], "");
            } else if (split[0].contains("(Source:")) {
                String sectionName = StringUtils.substringBefore(split[0], "(Source:");
                statut.setSectionName(sectionName.replaceAll(",,", ""));
                citation = citation.replace(sectionName, "");
            } else {
                statut.setSectionName(split[0].replaceAll(",,", ""));
                citation = citation.replace(split[0], "");
            }

            Pattern pattern = Pattern.compile("\\(Source:[A-Za-z0-9; ,\\-\\.\\/]{5,50}\\)");
            Matcher matcher = pattern.matcher(citation);
            if (matcher.find()) {

                String note = matcher.group();
                citation = StringUtils.substringBefore(citation, note);
                if (note.charAt(0) == '(' && note.charAt(note.length() - 1) == ')')
                    statut.setNoteSource(note.substring(1, note.length() - 2));
            }


            statut.setParagraphs(citation);
            statut.setCurrentLevel(statut.getCode());
//            statut.setParentLevel(fourthLevel.getId().getName());
//
            if (statut.getSectionName().startsWith(")")) {
                statut.setSectionName(StringUtils.substring(statut.getSectionName(), 2));
            }

            System.out.println("Section:\t" + statut.getSectionName());
            System.out.println("NoteFrom:\t" + statut.getNoteFrom());
            System.out.println("NoteSource:\t" + statut.getNoteSource());
            System.out.println();
            statut.setSectionName(statut.getSectionName().replaceAll("\\s+", " "));
            if (statut.getNoteFrom() != null)
                statut.setNoteFrom(statut.getNoteFrom().replaceAll("(,, *)+", " "));
            if (statut.getNoteSource() != null)
                statut.setNoteSource(statut.getNoteSource().replaceAll("(,, *)+", " "));
            if (statut.getParagraphs() != null) {
                statut.setParagraphs(statut.getParagraphs().replaceAll("(,, *)+", "!!delimeter!!"));
            }

            if (statut.getCode().contains("SUBSECTION")) {
                continue;
            }
//            statut.setLink(fourthLevel.getLink());
            statutes.add(statut);
        }

        if (statutes.isEmpty()) {
            return;
        }
    }

    @Autowired
    private Scraper scraper;

    @Test
    public void scraperTest() throws IOException {
        scraper.scrape();
    }
}