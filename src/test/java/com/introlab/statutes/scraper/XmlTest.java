package com.introlab.statutes.scraper;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author vitalii.
 */
public class XmlTest {

    @Test
    public void test() throws IOException {
        Document doc = new Document();

        Namespace namespace = Namespace.getNamespace("stat", "https://casetext.com/stat");


        Element root = new Element("Document", namespace);
        root.setAttribute("id", java.util.UUID.randomUUID().toString());
        Element child1 = new Element("Title", namespace);
        child1.addContent("Illinois Compiled Statutes");
        Element child2 = new Element("URL", namespace);
        child2.addContent("http://www.ilga.gov/legislation/ilcs/ilcs.asp");
        Element child3 = new Element("Version", namespace);
        child3.addContent("0.1");
        Element child4 = new Element("Currency", namespace);
        child4.addContent("2017-11-13");


        Element child5 = new Element("Level", namespace);
        child5.setAttribute("id", java.util.UUID.randomUUID().toString());
        Element child6 = new Element("Text", namespace);
        child5.addContent(child6);
        Element child7 = new Element("heading", namespace);
        child6.addContent(child7);
        child7.addContent("AGRICULTURE AND CONSERVATION");

        root.addContent(child1);
        root.addContent(child2);
        root.addContent(child3);
        root.addContent(child4);
        root.addContent(child5);

        doc.setRootElement(root);

        XMLOutputter outter = new XMLOutputter();
        outter.setFormat(Format.getPrettyFormat());
        outter.output(doc, new FileWriter(new File("myxml.xml")));
    }
}
